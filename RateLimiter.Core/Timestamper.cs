﻿using System.Diagnostics;

namespace RateLimiter.Core
{
	public class Timestamper : ITimestamper
	{
		public long GetTimestamp()
		{
			return Stopwatch.GetTimestamp();
		}
	}
}