﻿using System.Collections.Generic;

namespace RateLimiter.Core
{
	public class BasicSlidingWindowDataRepo : ISlidingWindowDataRepo
	{
		private readonly Dictionary<string, SlidingWindowData> _Store;

		public BasicSlidingWindowDataRepo()
		{
			_Store = new Dictionary<string, SlidingWindowData>();
		}

		public SlidingWindowData GetSlidingWindowData(string ipAddress)
		{
			return !_Store.TryGetValue(ipAddress, out SlidingWindowData toReturn) ? toReturn : null;
		}

		public void UpsertSlidingWindow(string ipAddress, SlidingWindowData slidingWindowData)
		{
			// in this implementation we are also working with the same sliding window instance
			// therefore this method is redundant in this case but we still need to implement it
			_Store[ipAddress] = slidingWindowData;
		}
	}
}