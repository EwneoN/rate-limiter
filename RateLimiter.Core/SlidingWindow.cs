﻿namespace RateLimiter.Core
{
	public class SlidingWindow : SlidingWindowBase
	{
		private long? _WindowStartTimestamp;
		private int _PreviousRequestCount;
		private int _RequestCount;

		public SlidingWindow(string ipAddress, ITimestamper timestamper, int requestLimit, int requestIntervalMilliseconds) 
			: base(ipAddress, timestamper, requestLimit, requestIntervalMilliseconds)
		{ }

		protected override SlidingWindowData GetData()
		{
			return new SlidingWindowData
			{
				WindowStartTimestamp = _WindowStartTimestamp,
				PreviousRequestCount = _PreviousRequestCount,
				RequestCount = _RequestCount
			};
		}

		protected override void SetData(SlidingWindowData data)
		{
			_WindowStartTimestamp = data.WindowStartTimestamp;
			_PreviousRequestCount = data.PreviousRequestCount;
			_RequestCount = data.RequestCount;
		}
	}
}
