﻿using System;

namespace RateLimiter.Core
{
	public class RateLimitException : Exception
	{
		public RateLimitException(string ipAddress) : base($"{ipAddress} has reached rate limit") { }
	}
}