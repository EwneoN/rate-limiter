﻿using System;
using System.Collections.Generic;

namespace RateLimiter.Core
{
	public class RateLimiter
	{
		private readonly object _CacheLocker;
		private readonly Dictionary<string, SlidingWindow> _Cache;
		private readonly ITimestamper _Timestamper;
		private readonly int _RequestLimit;
		private readonly int _RequestIntervalMilliseconds;

		public RateLimiter(ITimestamper timestamper, int requestLimit, int requestIntervalMilliseconds)
		{
			_Timestamper = timestamper ?? throw new ArgumentNullException(nameof(timestamper));
			_RequestLimit = requestLimit;
			_RequestIntervalMilliseconds = requestIntervalMilliseconds;

			_CacheLocker = new object();
			_Cache = new Dictionary<string, SlidingWindow>();
		}

		public void ProcessRequest(string ipAddress)
		{
			SlidingWindow window;

			lock (_CacheLocker)
			{
				if (!_Cache.TryGetValue(ipAddress, out window))
				{
					window = new SlidingWindow(ipAddress, _Timestamper, _RequestLimit, _RequestIntervalMilliseconds);
					
					_Cache.Add(ipAddress, window);
				}
			}

			if (!window.IsRequestValid())
			{
				throw new RateLimitException(ipAddress);
			}
		}
	}
}
