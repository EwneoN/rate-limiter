﻿namespace RateLimiter.Core
{
	public interface ITimestamper
	{
		long GetTimestamp();
	}
}
