﻿using System;

namespace RateLimiter.Core
{
	public class PersistedSlidingWindow : SlidingWindowBase
	{
		private readonly ISlidingWindowDataRepo _SlidingWindowDataRepo;

		public PersistedSlidingWindow(string ipAddress, ITimestamper timestamper,
			ISlidingWindowDataRepo slidingWindowDataRepo, int requestLimit, int requestIntervalMilliseconds) 
			: base(ipAddress, timestamper, requestLimit, requestIntervalMilliseconds)
		{
			_SlidingWindowDataRepo = slidingWindowDataRepo ?? throw new ArgumentNullException(nameof(slidingWindowDataRepo));
		}

		protected override SlidingWindowData GetData()
		{
			return _SlidingWindowDataRepo.GetSlidingWindowData(IpAddress) ?? new SlidingWindowData();
		}

		protected override void SetData(SlidingWindowData data)
		{
			_SlidingWindowDataRepo.UpsertSlidingWindow(IpAddress, data);
		}
	}
}