﻿namespace RateLimiter.Core
{
	public class SlidingWindowData
	{
		public long? WindowStartTimestamp { get; set; }
		public int PreviousRequestCount { get; set; }
		public int RequestCount { get; set; }
	}
}