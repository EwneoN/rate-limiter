﻿namespace RateLimiter.Core
{
	public interface ISlidingWindowDataRepo
	{
		SlidingWindowData GetSlidingWindowData(string ipAddress);
		void UpsertSlidingWindow(string ipAddress, SlidingWindowData slidingWindowData);
	}
}
