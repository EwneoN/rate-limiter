﻿using System;

namespace RateLimiter.Core
{
	public abstract class SlidingWindowBase
	{
		private readonly object _Locker;
		private readonly ITimestamper _Timestamper;
		private readonly long _RequestIntervalTicks;
		private readonly int _RequestLimit;

		public string IpAddress { get; }

		protected SlidingWindowBase(string ipAddress, ITimestamper timestamper, int requestLimit, int requestIntervalMilliseconds)
		{
			if (string.IsNullOrWhiteSpace(ipAddress))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(ipAddress));
			}

			if (requestLimit < 1)
			{
				throw new ArgumentOutOfRangeException(nameof(requestLimit));
			}

			if (requestIntervalMilliseconds <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(requestIntervalMilliseconds));
			}

			_Locker = new object();

			IpAddress = ipAddress;
			_Timestamper = timestamper ?? throw new ArgumentNullException(nameof(timestamper));
			_RequestLimit = requestLimit;
			_RequestIntervalTicks = requestIntervalMilliseconds * TimeSpan.TicksPerMillisecond;
		}

		public bool IsRequestValid()
		{
			bool isValidRequest;

			lock (_Locker)
			{
				SlidingWindowData data = GetData();

				long? windowStartTimestamp = data.WindowStartTimestamp;
				int previousRequestCount = data.PreviousRequestCount;
				int requestCount = data.RequestCount;

				long currentTime = _Timestamper.GetTimestamp();
				long elapsedTime = windowStartTimestamp.HasValue
					? (currentTime - windowStartTimestamp.Value)
					: 0;

				if (windowStartTimestamp.HasValue)
				{
					// window exists
					if (elapsedTime >= _RequestIntervalTicks)
					{
						if (elapsedTime > _RequestIntervalTicks * 2)
						{
							// last window had no requests, therefore we reset the state
							windowStartTimestamp = currentTime;
							previousRequestCount = 0;
							requestCount = 0;

							elapsedTime = 0;
						}
						else
						{
							// we need to create a new window as the old window has ended
							windowStartTimestamp += _RequestIntervalTicks;
							previousRequestCount = requestCount;
							requestCount = 0;

							elapsedTime = currentTime - windowStartTimestamp.Value;
						}
					}
				}
				else
				{
					// no window exists, start a new one
					windowStartTimestamp = currentTime;
				}

				double weightedRequestCount =
					previousRequestCount * ((double) (_RequestIntervalTicks - elapsedTime) / _RequestIntervalTicks) +
					requestCount + 1;

				isValidRequest = weightedRequestCount <= _RequestLimit;

				if (isValidRequest)
				{
					requestCount++;
				}

				SetData(new SlidingWindowData
				{
					WindowStartTimestamp = windowStartTimestamp,
					PreviousRequestCount = previousRequestCount,
					RequestCount = requestCount
				});
			}

			return isValidRequest;
		}

		protected abstract SlidingWindowData GetData();

		protected abstract void SetData(SlidingWindowData data);
	}
}