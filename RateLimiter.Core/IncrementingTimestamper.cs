﻿using System;

namespace RateLimiter.Core
{
	public class IncrementingTimestamper : ITimestamper
	{
		private long _Timestamp;
		private readonly long _IncrementSizeMilliseconds;

		public IncrementingTimestamper(long timestamp, long incrementSizeMilliseconds)
		{
			//at least 1970-1-1
			if (timestamp < 621355968000000000)
			{
				throw new ArgumentOutOfRangeException(nameof(timestamp));
			}

			_Timestamp = timestamp;
			_IncrementSizeMilliseconds = incrementSizeMilliseconds;
		}

		public long GetTimestamp()
		{
			long timestamp = _Timestamp;
			_Timestamp += _IncrementSizeMilliseconds * TimeSpan.TicksPerMillisecond;
			return timestamp;
		}

		public void IncrementManually(long milliseconds)
		{
			_Timestamp += milliseconds * TimeSpan.TicksPerMillisecond;
		}
	}
}
