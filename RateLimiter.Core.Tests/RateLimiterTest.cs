using System;
using FluentAssertions;
using Xunit;

namespace RateLimiter.Core.Tests
{
	public class RateLimiterTest
	{
		private const string _IpAddress = "192.168.0.10";
		private const int _RequestLimit = 5;
		private const int _RequestLimitWindowInMilliseconds = 5000;
		private const long _TimeBetweenRequests = 100;

		[Fact]
		public void ShouldNotThrowExceptionIfRateLimitHasNotBeenReached()
		{
			// Arrange
			DateTimeOffset now = new DateTimeOffset(2021, 1, 1, 12, 0, 0, TimeSpan.FromHours(10));

			IncrementingTimestamper timestamper = new IncrementingTimestamper(now.Ticks, _TimeBetweenRequests);
			RateLimiter rateLimiter = new RateLimiter(timestamper, _RequestLimit, _RequestLimitWindowInMilliseconds);
			
			// Act
			Action act = () => rateLimiter.ProcessRequest(_IpAddress);

			// Assert
			act
				.Should()
				.NotThrow<RateLimitException>();
		}

		[Fact]
		public void ShouldThrowExceptionIfRateLimitHasBeenReached()
		{
			// Arrange
			DateTimeOffset now = new DateTimeOffset(2021, 1, 1, 12, 0, 0, TimeSpan.FromHours(10));

			IncrementingTimestamper timestamper = new IncrementingTimestamper(now.Ticks, _TimeBetweenRequests);
			RateLimiter rateLimiter = new RateLimiter(timestamper, _RequestLimit, _RequestLimitWindowInMilliseconds);

			// Act
			Action act = () =>
			{
				//loop to limit + 1
				for (int i = 0; i < _RequestLimit + 1; i++)
				{
					rateLimiter.ProcessRequest(_IpAddress);
				}
			};

			// Assert
			act
				.Should()
				.Throw<RateLimitException>();
		}
	}
}
