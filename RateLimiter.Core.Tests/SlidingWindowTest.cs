using System;
using FluentAssertions;
using Xunit;

namespace RateLimiter.Core.Tests
{
	public class SlidingWindowTest
	{
		private const string _IpAddress = "192.168.0.10";
		private const int _RequestLimit = 5;
		private const int _RequestLimitWindowInMilliseconds = 5000;
		private const long _TimeBetweenRequests = 100;

		[Fact]
		public void RequestShouldBeValidIfRateLimitHasNotBeenReached()
		{
			// Arrange
			DateTimeOffset now = new DateTimeOffset(2021, 1, 1, 12, 0, 0, TimeSpan.FromHours(10));

			IncrementingTimestamper timestamper = new IncrementingTimestamper(now.Ticks, _TimeBetweenRequests);

			SlidingWindow slidingWindow = new SlidingWindow(_IpAddress, timestamper, _RequestLimit, _RequestLimitWindowInMilliseconds);
			bool? requestIsValid = null;

			// Act
			Action act = () => requestIsValid = slidingWindow.IsRequestValid();

			// Assert
			act
				.Should()
				.NotThrow();

			requestIsValid
				.Should()
				.NotBeNull()
				.And
				.BeTrue();
		}

		[Fact]
		public void RequestShouldNotBeValidIfRateLimitHasBeenReached()
		{
			// Arrange
			DateTimeOffset now = new DateTimeOffset(2021, 1, 1, 12, 0, 0, TimeSpan.FromHours(10));

			IncrementingTimestamper timestamper = new IncrementingTimestamper(now.Ticks, _TimeBetweenRequests);

			SlidingWindow slidingWindow = new SlidingWindow(_IpAddress, timestamper, _RequestLimit, _RequestLimitWindowInMilliseconds);
			bool? requestIsValid = null;

			// Act
			Action act = () =>
			{
				for (int i = 0; i < _RequestLimit; i++)
				{
					slidingWindow.IsRequestValid();
				}

				requestIsValid = slidingWindow.IsRequestValid();
			};

			// Assert
			act
				.Should()
				.NotThrow();

			requestIsValid
				.Should()
				.NotBeNull()
				.And
				.BeFalse();
		}

		[Fact]
		public void RequestShouldNotBeValidBecauseOfPreviousWindowWeight()
		{
			// Arrange
			DateTimeOffset now = new DateTimeOffset(2021, 1, 1, 12, 0, 0, TimeSpan.FromHours(10));

			IncrementingTimestamper timestamper = new IncrementingTimestamper(now.Ticks, _TimeBetweenRequests);

			SlidingWindow slidingWindow = new SlidingWindow(_IpAddress, timestamper, _RequestLimit, _RequestLimitWindowInMilliseconds);
			bool? requestIsValid = null;

			// Act
			Action act = () =>
			{
				for (int i = 0; i < _RequestLimit; i++)
				{
					slidingWindow.IsRequestValid();
				}

				//simulate a pause size of the the window size
				timestamper.IncrementManually(_RequestLimitWindowInMilliseconds);

				requestIsValid = slidingWindow.IsRequestValid();
			};

			// Assert
			act
				.Should()
				.NotThrow();

			requestIsValid
				.Should()
				.NotBeNull()
				.And
				.BeFalse();
		}

		[Fact]
		public void RequestShouldBeValidBecauseOfPreviousWindowWeightIfEnoughTimeHasElapsed()
		{
			// Arrange
			DateTimeOffset now = new DateTimeOffset(2021, 1, 1, 12, 0, 0, TimeSpan.FromHours(10));

			IncrementingTimestamper timestamper = new IncrementingTimestamper(now.Ticks, _TimeBetweenRequests);

			SlidingWindow slidingWindow = new SlidingWindow(_IpAddress, timestamper, _RequestLimit, _RequestLimitWindowInMilliseconds);
			bool? requestIsValid = null;

			// Act
			Action act = () =>
			{
				for (int i = 0; i < _RequestLimit; i++)
				{
					slidingWindow.IsRequestValid();
				}

				//simulate a pause 1.8 times the window size
				timestamper.IncrementManually((long)(_RequestLimitWindowInMilliseconds * 1.8));

				requestIsValid = slidingWindow.IsRequestValid();
			};

			// Assert
			act
				.Should()
				.NotThrow();

			requestIsValid
				.Should()
				.NotBeNull()
				.And
				.BeTrue();
		}

		[Fact]
		public void RequestShouldBeValidBecauseOfWindowReset()
		{
			// Arrange
			DateTimeOffset now = new DateTimeOffset(2021, 1, 1, 12, 0, 0, TimeSpan.FromHours(10));

			IncrementingTimestamper timestamper = new IncrementingTimestamper(now.Ticks, _TimeBetweenRequests);

			SlidingWindow slidingWindow = new SlidingWindow(_IpAddress, timestamper, _RequestLimit, _RequestLimitWindowInMilliseconds);
			bool? requestIsValid = null;

			// Act
			Action act = () =>
			{
				for (int i = 0; i < _RequestLimit; i++)
				{
					slidingWindow.IsRequestValid();
				}

				//simulate a pause twice the size of the window size
				timestamper.IncrementManually(_RequestLimitWindowInMilliseconds * 2);

				for (int i =0; i < _RequestLimit; i++)
				{
					requestIsValid = slidingWindow.IsRequestValid();
				}
			};

			// Assert
			act
				.Should()
				.NotThrow();

			requestIsValid
				.Should()
				.NotBeNull()
				.And
				.BeTrue();
		}
	}
}